#include <iostream>

class A
{
public:
	int a;
	virtual int f()
	{
		return 5;
	}
};

class B : public A
{
public:
	virtual int f()
	{
		return 6;
	}
};

class C : public B
{
	int f()
	{
		return 7;
	}
};

typedef int func(void);
int CallF(A* a)
{
	int* b = (int*)((void**)a + 0);
	int* c = (int*)((void**)*b + 0);
	return ((func*)(*c))();
}

int main()
{
	A* a = new A();
	A* b = new B();
	A* c = new C();
	std::cout << CallF(a) << std::endl;
	std::cout << CallF(b) << std::endl;
	std::cout << CallF(c) << std::endl;

}